
import React, { useEffect, useState } from 'react'

import Cookies from 'js-cookie'
import { useDispatch } from 'react-redux'
import { LazyLoadImage } from 'react-lazy-load-image-component'

import SearchActions from '../../../redux/reducer/SearchRedux'

import config from '../../../../../config'
import { GetMobileAppsLink } from '../../../utils/misc'
import SearchBarContainer from '../mobile/SearchBarContainer'
import TopMenu from './organism/TopMenu'
import BottomMenu from './organism/BottomMenu'

export default function HeaderContainer () {
//   const context = useContext(LayoutContext)
  // need to create two type of header
  //! default and logo only

  const [keyword, setKeyword] = useState('')
  const [isClickedSearchBar, setIsClickedSearchBar] = useState(false)

  const [offsetY, setOffsetY] = useState(0)

  const [bannerCampaignApps, setBannerCampaignApps] = useState(false)
  // const [ruppersPreference] = useState(Cookies.get('ruppers-preference'))
  const dispatch = useDispatch()
  const ruppersPreference = Cookies.get('ruppers-preference')
  const handleSearchOnType = (text) => {
    setKeyword(text)
    dispatch(SearchActions.searchByKeywordAlgoliaRequest(text))
  }

  useEffect(() => {
    if (!bannerCampaignApps && ruppersPreference) {
      setBannerCampaignApps(true)
    }
  }, [ruppersPreference])

  useEffect(() => {
    window.onscroll = function () {
      setOffsetY(window.pageYOffset)
    }
  }, [])

  const handleOpenMobileApps = (e) => {
    e.preventDefault()
    window.location = GetMobileAppsLink(config.companyCode)
  }

  const handleCloseCampaignAppsBanner = (e) => {
    e.preventDefault()
    setBannerCampaignApps(false)
  }

  return (
    <>
      <div className='menu__mobile menu__mobile--top fixed'>
        {bannerCampaignApps &&
          <div className='col-xs-12 padding__horizontal--none'>
            <div className='mobileApps'>
              <div className='col'>
                <div className='row'>
                  <div className='col-xs-1 icon'>
                    <LazyLoadImage src={`${config.assetsURL}icon/price-tag.svg`} className='mt-0' alt='Price Tag Icon' height={24} width={24} />
                  </div>
                  <div className='col-xs-8 heading-4 margin-left-xs text-left'>Belanja lebih mudah dan praktis dengan aplikasi Ruparupa</div>
                  <div className='col-xs-2 margin-left-xs button'>
                    <button className='btn btn-full btn-primary-border btn-s button-small-text' onClick={(e) => handleOpenMobileApps(e)}>Buka</button>
                  </div>
                  <div className='col-xs-1 margin-left-xs icon' onClick={(e) => handleCloseCampaignAppsBanner(e)}>
                    <LazyLoadImage src={`${config.assetsURL}icon/close-campaign-banner.svg`} className='mt-0' alt='Close Icon' height={24} width={24} />
                  </div>
                </div>
              </div>
            </div>
          </div>}
        <TopMenu
          keyword={keyword}
          offsetY={offsetY}
          isClickedSearchBar={isClickedSearchBar}
          setKeyword={setKeyword}
          setIsClickedSearchBar={setIsClickedSearchBar}
          handleSearchOnType={handleSearchOnType}
        />
        {isClickedSearchBar && (
          <div className='col-xs-12'>
            <SearchBarContainer keyword={keyword} isClickedSearchBar={isClickedSearchBar} />
          </div>
        )}
      </div>
      <BottomMenu />
    </>
  )
}
