import React, { useContext, useEffect, useState } from 'react'
import dynamic from 'next/dynamic'
import isEmpty from 'lodash/isEmpty'
import Cookies from 'js-cookie'
// import config from '../../../config'
import HeadConfigScript from '../../homepage/Containers/HeadConfigScript'
import Emarsys from '../../homepage/Containers/Emarsys'
import { LayoutContext } from '../../global/context/LayoutContext'
import FooterContainer from './FooterContainer'
import config from '../../../config'

const RuppersPreferenceModal = dynamic(() => import('../../../src/Components/Modal/RuppersPreferenceModal'))
const Modal = dynamic(() => import('../../global/layouts/Modal'), { ssr: false })

export default function DefaultLayout ({ children, pageFrom }) {
  const context = useContext(LayoutContext)
  const { query } = context
  const [onClickedRuppersPreferenceModal, setOnClickedRuppersPreferenceModal] = useState(false)
  //   const urlKey = (url && url.asPath) ? url.asPath.substr(1) : ''
  // Uncomment line 23 & 24 for development purpose, copy ProductLastSeenData
  // const productLastSeen =
  // localforage.setItem('product_last_seenv2', productLastSeen)

  useEffect(() => {
    window.addEventListener('scroll', handleOpenRuppersPreferenceModal, { passive: true })
    return () => window.removeEventListener('scroll', handleOpenRuppersPreferenceModal)
  }, [])

  useEffect(() => {
    const otpToken = Cookies.get('rpcn')
    if (query) {
      if (isEmpty(otpToken)) {
        Cookies.set('rpcn', query.otpToken)
      }
    }
  }, [query])

  const handleOpenRuppersPreferenceModal = () => {
    const ruppersPreference = Cookies.get('ruppers-preference')
    if (!ruppersPreference) {
      setOnClickedRuppersPreferenceModal(true)
    }
  }

  // const handleCloseRuppersPreferenceModal = (e) => {
  //   e.preventDefault()
  //   Cookies.set('ruppers-preference', 'web', { expires: 432000 })
  // }

  const ruppersPreferenceModalBody = (functionInsideModal) => <RuppersPreferenceModal onClose={functionInsideModal} />

  return (
    <div>
      {/* <HeaderContainer /> */}
      <HeadConfigScript />
      {children}
      <FooterContainer />
      {onClickedRuppersPreferenceModal &&
        <Modal
          bodyClass='modal-body-apps-preference'
          contentClass='modal-content-apps-preference'
          dialogClass='modal-dialog-apps-preference'
          bodyElement={ruppersPreferenceModalBody}
          modalType='bottom'
          onClose={() => setOnClickedRuppersPreferenceModal(false)}
          title='My Modal'
          show={onClickedRuppersPreferenceModal}
        />}
      {(config?.emarsysScript !== '' && config.targetVisitor === 'human') && <Emarsys />}
    </div>
  )
}
