import React from 'react'
import { LazyLoadImage } from 'react-lazy-load-image-component'
import config from '../../../../config'

export default function FooterSosmed () {
  return (
    <div className='sosmed-ruparupa'>
      <div className='row'>
        <div className='col-xs-12'>
          <LazyLoadImage src={`${config.assetsURL}ruparupa-logo.svg`} height={24} width={133} />
        </div>
        <div className='col-xs-12'>
          <span className='text'>Situs Furnitur Terlengkap dan Terpercaya</span>
        </div>
        <div className='col-xs-12'>
          <div className='sosmed-ruparupa__container-logo'>
            <div className='logo'>
              <LazyLoadImage src={`${config.assetsURL}icon/footer/twitter.svg`} height={16} width={20} />
            </div>
            <div className='logo'>
              <LazyLoadImage src={`${config.assetsURL}icon/footer/instagram.svg`} height={16} width={20} />
            </div>
            <div className='logo'>
              <LazyLoadImage src={`${config.assetsURL}icon/footer/facebook.svg`} height={16} width={20} />
            </div>
            <div className='logo'>
              <LazyLoadImage src={`${config.assetsURL}icon/footer/linkedin.svg`} height={16} width={20} />
            </div>
            <div className='logo'>
              <LazyLoadImage src={`${config.assetsURL}icon/footer/youtube.svg`} height={16} width={20} />
            </div>
            <div className='logo'>
              <LazyLoadImage src={`${config.assetsURL}icon/footer/pinterest.svg`} height={16} width={20} />
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
