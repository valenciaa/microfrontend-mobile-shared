import React from 'react'
import config from '../../../../config'
import Accordion from '../../../global/layouts/moleculs/Accordion'

export default function FooterAccordion () {
  const linkData = [
    {
      url: 'https://www.ruparupa.com/blog/',
      title: 'Blog'
    },
    {
      url: 'https://m.ruparupa.com/faq-pengembalian#content-1',
      title: 'Cara Belanja'
    },
    {
      url: 'https://m.ruparupa.com/payment',
      title: 'Pembayaran'
    },
    {
      url: 'https://m.ruparupa.com/campaign/seller-center/',
      title: 'Pendaftaran Seller'
    },
    {
      url: 'https://m.ruparupa.com/faq-pengembalian',
      title: 'Pengembalian'
    },
    {
      url: config.baseURL + 'how-to-pickup',
      title: 'Pengiriman & Pengambilan Barang'
    },
    {
      url: 'https://m.ruparupa.com' + config.entryPoint + '/faq',
      title: 'FAQ'
    },
    {
      url: 'https://www.ruparupa.com/gratis-ongkir',
      title: 'Gratis Ongkir'
    },
    {
      url: 'https://www.ruparupa.com/program-cicilan',
      title: 'Program Cicilan'
    },
    {
      url: 'https://www.ruparupa.com/b2b',
      title: 'Solusi Bisnis dan Usaha'
    },
    {
      url: 'https://www.ruparupa.com/informa/custom-furniture',
      title: 'Custom Furniture'
    }

  ]
  function consumenService () {
    return (
      <ul>
        {linkData?.map((data, index) => {
          let show = true
          if (data.title === 'Pendaftaran Seller')show = false
          if (show) {
            return (
              <li key={index}>
                <a href={data.url} target='_blank' rel='nofollow noreferrer noopener'>{data.title}</a>
              </li>
            )
          }
        })}
      </ul>
    )
  }

  function contactUs () {
    return (
      <ul>
        <li><a href='mailto:help@ruparupa.com' rel='nofollow'><i className='anticon icon-mail' /> help@ruparupa.com & Live Chat (24/7)</a></li>
        <li><a href='tel:+622129670706' target='_blank' rel='nofollow noopener noreferrer'><i className='anticon icon-phone' /> 021 - 2967 0706</a></li>
        <li>Senin - Jumat 09.00 - 18.00 WIB (kecuali hari libur nasional)</li>
      </ul>)
  }
  return (
    <div>
      <Accordion
        title='Layanan Konsumen'
        content={consumenService()}
      />
      <Accordion
        title='Tentang Ruparupa'
        content=''
        url='https://www.ruparupa.com/about-us'
      />
      <Accordion
        title='Kontak Kami'
        content={contactUs()}
      />
    </div>
  )
}
