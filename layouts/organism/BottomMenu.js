import React, { useContext } from 'react'
import ToogleImage from '../moleculs/ToogleImage'

import { LayoutContext } from '../../../global/context/LayoutContext'
import config from '../../../../config'

export default function BottomMenu () {
  const context = useContext(LayoutContext)

  return (
    <div className='menu__mobile menu__mobile--bottom'>
      <div className='col'>
        <ToogleImage
          type='header-bottom'
          primaryImage={`${config.assetsURL}icon/home-active.svg`}
          alt='Home'
          link='/'
          className={`home-link ${context?.pageFrom === 'home' ? 'home-link__active' : ''}`}
          title='Beranda'
        />
        {context?.pageFrom === 'home' && <hr />}
      </div>
      <div className='col'>
        <ToogleImage
          type='header-bottom'
          primaryImage={`${config.assetsURL}icon/inspiration.svg`}
          alt='Inspirasi'
          link={`${config.legacyBaseURL}dashboard?tab=my-orders`}
          className={`home-link ${context?.pageFrom === 'inspiration' ? 'home-link__active' : ''}`}
          title='Inspirasi'
        />
      </div>
      <div className='col'>
        <ToogleImage
          type='header-bottom'
          primaryImage={`${config.assetsURL}icon/transaction.svg`}
          alt='Transaksi'
          link={`${config.legacyBaseURL}dashboard?tab=my-orders`}
          className={`home-link ${context?.pageFrom === 'transaction' ? 'home-link__active' : ''}`}
          title='Transaksi'
        />
      </div>
      <div className='col'>
        <ToogleImage
          type='header-bottom'
          primaryImage={`${config.assetsURL}icon/account.svg`}
          alt='Account'
          link='/'
          className={`home-link ${context?.pageFrom === 'account' ? 'home-link__active' : ''}`}
          title='Akun saya'
        />
      </div>
    </div>
  )
}
