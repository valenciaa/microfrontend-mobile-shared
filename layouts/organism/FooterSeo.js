import React, { useRef, useState } from 'react'

export default function FooterSeo () {
    const [showCollapse, setShowCollapse] = useState('preview-seo')
    const [setHeight, setHeightState] = useState("328px");
    const content = useRef(null)
    const text = 
    `<p>Ruparupa merupakan One Stop Online Shopping yang hadir untuk menjawab kebutuhan masyarakat modern akan kemudahan berbelanja, kapanpun dan dimanapun Anda menginginkannya. Kami berusaha membantu Anda dalam bertransaksi dan menikmati gaya hidup modern yang praktis melalui berbagai produk eksklusif berkualitas premium dengan harga yang kompetitif.</p><br>
    <p>Selain itu, kami juga memberikan fasilitas cicilan 0% yang bisa Anda nikmati melalui kerja sama dengan berbagai kartu kredit terkemuka.</p><br>
    <p>Ruparupa berkomitmen untuk memberikan pengalaman belanja online yang aman dan nyaman dengan jaminan orisinalitas untuk semua produk yang kami jual, transaksi dengan proses yang cepat dan mudah, fasilitas penukaran dan pengembalian produk, garansi resmi dari vendor-vendor terkemuka.</p><br>
    <p>Untuk kemudahan Anda, Ruparupa juga menghadirkan fitur pengambilan langsung di beberapa pick up point yang berlokasi di berbagai tempat strategis di Indonesia serta beragam pilihan fasilitas pembayaran yang lengkap, mudah dan aman. Ruparupa memberikan layanan terbaik dalam belanja online dengan berbagai produk berkualitas mulai dari furniture, dekorasi rumah, kebutuhan olahraga, perlengkapan rumah tangga, peralatan dapur, aksesori, perkakas, serta hobi dan gaya hidup.</p><br>
    <p>Ruparupa merupakan bagian dari Kawan Lama Group yang telah berpengalaman selama puluhan tahun dalam bisnis retail dan membawahi sejumlah anak perusahaan antara lain Ace Hardware, Informa Furnishings dan Toys Kingdom. Selamat menikmati pengalaman berbelanja yang menyenangkan hanya di Ruparupa!</p><br>`
    const toggleHidden = () => {
        setShowCollapse(showCollapse === 'preview-seo' ? "show-seo" : 'preview-seo')
        setHeightState(
          showCollapse === "show-seo" ? "328px" : `${content.current.scrollHeight + 50}px`
        );
      }
    return (
    <div className='collapsable'> 
        <div className='footer-index text-center'>
          <div
            ref={content}
            className={`container ${(showCollapse)} text-justify`}
            style={{ height: `${setHeight}` }}
            className="collapsable__content"
          >
            <div className='collapsable__title'><b>Ruparupa - Belanja Online Sederhana</b></div>
            <div
              className='collapsable__text'
              dangerouslySetInnerHTML={{ __html: text }}
            />
          </div>
        </div>
        <div className='text-center collapsable-seo'>
          <button className=''onClick={() => toggleHidden()}>Baca Selengkapnya</button>
        </div>
      </div>
    )
}
