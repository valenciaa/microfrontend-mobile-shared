import React from 'react'
import { LazyLoadImage } from 'react-lazy-load-image-component'
import config from '../../../../config'
import ToogleImage from '../../../global/layouts/moleculs/ToogleImage'

export default function TopMenu ({ keyword, isClickedSearchBar, offsetY, setKeyword, setIsClickedSearchBar, handleSearchOnType }) {
  const condition = offsetY < 3 && isClickedSearchBar === false
  return (
    <div className='col-xs-12 padding__horizontal--none' style={condition ? { backgroundColor: 'transparent' } : { backgroundColor: 'white' }}>
      <div className='row middle-xs'>
        <div className='col-xs-8  padding padding__vertical--xs '>
          <div className='search-input'>
            <input className='search-bar' placeholder='Cari di' value={keyword} onClick={() => setIsClickedSearchBar(true)} onBlur={() => setIsClickedSearchBar(false)} onChange={(e) => handleSearchOnType(e.target.value)} />
            <ToogleImage type='header-top' primaryImage={`${config.assetsURL}icon/search-primary.svg`} alt='Search' condition height={16} width={16} className='search-icon__mobile' />
            {keyword.length > 0 && <LazyLoadImage src={`${config.assetsURL}icon/close-fill.svg`} className='close-icon__mobile' onClick={() => setKeyword('')} alt='Search Icon' height={16} width={16} />}
          </div>
        </div>
        <ToogleImage type='header-top' primaryImage={`${config.assetsURL}icon/wishlist-primary.svg`} secondaryImage={`${config.assetsURL}icon/wishlist-secondary.svg`} alt='Wishlist' condition={condition} />
        <ToogleImage type='header-top' primaryImage={`${config.assetsURL}icon/notification-primary.svg`} secondaryImage={`${config.assetsURL}icon/notification-secondary.svg`} alt='Notification' condition={condition} />
        <ToogleImage type='header-top' primaryImage={`${config.assetsURL}icon/cart-primary.svg`} secondaryImage={`${config.assetsURL}icon/cart-secondary.svg`} alt='Cart' condition={condition} />
      </div>
    </div>
  )
}
