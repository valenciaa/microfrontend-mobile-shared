import React from 'react'
import config from '../../../config'
import FooterSeo from './organism/FooterSeo'
import FooterSosmed from './organism/FooterSosmed'
import FooterAccordion from './organism/FooterAccordion'
import FooterImages from '../../global/layouts/moleculs/FooterImages'
import { LazyLoadImage } from 'react-lazy-load-image-component'

export default function FooterContainer () {
  const data = {
    installment: [
      { title: 'BCA', url: 'bca' },
      { title: 'BNI', url: 'bni' },
      { title: 'BRI', url: 'bri' },
      { title: 'Bukopin Bank', url: 'bukopin' },
      { title: 'CIMB', url: 'cimb-bank-new' },
      { title: 'Danamon', url: 'danamon' },
      { title: 'Digibank', url: 'digibank-logo' },
      { title: 'HSBC', url: 'hsbc' },
      { title: 'Mandiri', url: 'mandiri' },
      { title: 'OCBC NISP', url: 'ocbc-nisp' },
      { title: 'Permata Bank', url: 'permata-bank' },
      { title: 'Bank Panin', url: 'panin-bank' },
      { title: 'Standard Charted', url: 'standard-charted' },
      { title: 'Bank Mega', url: 'bank-mega' },
      { title: 'Citibank', url: 'logo-citi-footer' },
      { title: 'Maybank', url: 'maybank' }
    ],
    courier_service: [
      { title: 'JNE', url: 'jne' },
      { title: 'GOSEND', url: 'gosend-logo-footer' },
      { title: 'NCS', url: 'ncs-logo' }
    ],
    secure_payment: [
      { title: 'VISA', url: 'verified-visa' },
      { title: 'Master Card Secure', url: 'mastercard-secure' },
      { title: 'Secured by SECTIGO', url: 'sectigo-small' }
    ],
    member_kawan_lama: [
      { title: 'Ace Hardware', url: 'ace-footer' },
      { title: 'Informa', url: 'informa-footer' },
      { title: 'Krisbow', url: 'krisbow-footer' },
      { title: 'Toys Kingdom', url: 'toyskingdom-footer' },
      { title: 'Pet Kingdom', url: 'footer-pet' },
      { title: 'Ataru', url: 'footer-ataru' },
      { title: 'Bico Fit', url: 'footer-bico' },
      { title: 'Dr Kong', url: 'footer-drkong' },
      { title: 'Pendopo', url: 'footer-pendopo' }
    ],
    payment_method: [
      { title: 'VISA', url: 'visa' },
      { title: 'Master Card', url: 'mastercard' },
      { title: 'Bank Transfer', url: 'bank-transfer' },
      { title: 'ATM Bersama', url: 'atm-bersama' },
      { title: 'OVO', url: 'ovo' },
      { title: 'Klik Pay', url: 'klik-pay' },
      { title: 'JCB', url: 'jcb' },
      { title: 'Gopay', url: 'gopay-new-logo-footer' }
    ]
  }
  if (config.companyCode === 'ODI') {
    data.member_kawan_lama.splice(1, 0, { title: 'Ruparupa', url: 'ruparupa-footer' })
  } else {
    data.member_kawan_lama.splice(data.member_kawan_lama.length - 1, 0, { title: 'Ruparupa', url: 'ruparupa-footer' })
  }
  // data.member_kawan_lama = config.companyCode === 'ODI' ? data.member_kawan_lama.splice(1, 0, { title: 'Ruparupa', url: 'ruparupa-footer' }) : data.member_kawan_lama.splice(data.member_kawan_lama.length - 1, 0, { title: 'Ruparupa', url: 'ruparupa-footer' })
  // const mixpanelTrackFooter = () => {
  //   mixpanel.track('Click Download ruparupa MobileApps', {
  //     Position: 'Footer',
  //     Device: config.platform,
  //     Source: config.companyName !== '' ? config.companyName : config.companyCode
  //   })
  // }

  return (
    <div className=''>
      <footer className='footer'>
        <FooterSeo />
        <hr className='bold' />
        <FooterSosmed />
        <hr className='bold' />
        <FooterAccordion />
        <div className='row'>
          <div className='col-xs-12'>
            <div className='footer-section__download-apps'>
              <div>Download Aplikasi Ruparupa</div>
              <div> <LazyLoadImage src={config.assetsURL + 'images/' + 'logo-app-store.svg'} height='39' width='122' alt='apps-store' /></div>
              <div> <LazyLoadImage src={config.assetsURL + 'images/' + 'logo-google-play.svg'} height='39' width='122' alt='google-play' /></div>
            </div>
          </div>
          <div className='col-xs-12'>
            <div className='footer-section__newsletter'>
              <div>Daftar Newsletter</div>
              <div>Jadilah orang pertama yang mendapatkan informasi diskon dan penawaran menarik dari Ruparupa</div>
              <div>
                <form>
                  <input className='input-box__with-button' type='email' placeholder='Email kamu di sini' />
                  <button className='button__primary button__primary--with-input '>Kirim</button>
                </form>
              </div>
            </div>
          </div>
          <div className='col-xs-12'>
            <FooterImages title='Cicilan 0%' data={data?.installment} />
          </div>
          <div className='col-xs-12'>
            <FooterImages title='Metode Pembayaran' data={data?.payment_method} />
          </div>
          <div className='col-xs-12'>
            <FooterImages title='Jasa Pengiriman' data={data?.courier_service} />
          </div>
        </div>
        <hr className='bold' />
        <div className='row'>
          <div className='col-xs-12'>
            <FooterImages title='' data={data?.member_kawan_lama} type='copyright' />
          </div>
        </div>
      </footer>
    </div>
  )
}
