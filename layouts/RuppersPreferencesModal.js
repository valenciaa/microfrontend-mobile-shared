import React from 'react'
import { LazyLoadImage } from 'react-lazy-load-image-component'
import config from '../../../config'
import GetMobileAppsLink from '../../../shared/global/layouts/Modal'
import Cookies from 'js-cookie'

export default function RuppersPreferenceModal ({ onClose }) {
  const handleSelectMobileApps = (e) => {
    e.preventDefault()
    handleCloseRuppersPreferenceModal()
    window.location = GetMobileAppsLink(config.companyCode)
  }

  const handleCloseRuppersPreferenceModal = (e) => {
    e.preventDefault()
    Cookies.set('ruppers-preference', 'web', { expires: 5 })
    onClose()
  }
  return (
    <>
      <div className='heading-2'>
        Gunakan Aplikasi {`${config.companyCode}` === 'AHI' ? 'MISS ACE' : 'Ruparupa'}?
      </div>
      <div className='col margin-top-m'>
        <div className='apps-preference-row row'>
          <div className='col-xs-2 margin-right-xs apps-preference-logo'>
            <LazyLoadImage src={`${config.companyCode === 'AHI' ? `${config.assetsURL}images/icon-miss-ace.png` : `${config.assetsURL}images/icon-ruparupa.png`}`} alt={`${config.companyCode === 'AHI' ? 'MISS ACE' : 'Ruparupa'}`} height={52} width={52} />
          </div>
          <div className='col-xs-7 ui-text-2 apps-preference-text'>
            Buka Aplikasi {`${config.companyCode}` === 'AHI' ? 'MISS ACE' : 'Ruparupa'}
          </div>
          <div className='col-xs-3 apps-preference-button'>
            <button className='btn btn-primary btn-full button-small-text' onClick={(e) => handleSelectMobileApps(e)}>Buka</button>
          </div>
        </div>
      </div>
      <div className='col margin-top-m'>
        <div className='apps-preference-row row'>
          <div className='col-xs-2 margin-right-xs apps-preference-logo'>
            <LazyLoadImage src={`${config.companyCode === 'AHI' ? `${config.assetsURL}images/icon-miss-ace.png` : `${config.assetsURL}images/icon-ruparupa.png`}`} alt={`${config.companyCode === 'AHI' ? 'MISS ACE' : 'Ruparupa'}`} height={52} width={52} />
          </div>
          <div className='col-xs-7 ui-text-2 apps-preference-text'>
            Tetap di Browser
          </div>
          <div className='col-xs-3 apps-preference-button'>
            <button className='btn btn-primary-border btn-full button-small-text' onClick={(e) => handleCloseRuppersPreferenceModal(e)}>Pilih</button>
          </div>
        </div>
      </div>
    </>
  )
}
