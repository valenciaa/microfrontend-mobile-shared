
import { useDispatch, useSelector } from 'react-redux'
import { useEffect, useState } from 'react'
import { useRouter } from 'next/router'
import startCase from 'lodash/startCase'
import toLower from 'lodash/toLower'

import url from 'url'

import SearchActions from '../../global/redux/reducer/SearchRedux'
import LogActions from '../../global/redux/reducer/LogRedux'
import config from '../../../../../config'


export default function SearchContainer (props) {
  const router = useRouter()

  const dispatch = useDispatch()
  const [isSubmitEvent, setIsSubmitEvent] = useState(false)

  const search = useSelector(state => state.search)
  const log = useSelector(state => state.log)

  const searchPageRedirectURL = search.pageRedirect ? search.pageRedirect : null
  const searchData = search.payload?.data?.data ? search.payload.data.data : null

  const searchProductByKeywordAlgolia = (keyword) => dispatch(SearchActions.searchByKeywordAlgoliaRequest(keyword))
  const searchPageRedirect = (keyword) => {
    setIsSubmitEvent(true)
    dispatch(SearchActions.searchPageRedirectRequest(encodeURIComponent(keyword)))
  }
  
  useEffect(() => {
    const itmFrom = ''
    if (!search.fetchingPageRedirect && isSubmitEvent) {
      setIsSubmitEvent(false)
      if (searchPageRedirectURL && searchPageRedirectURL.pageRedirect) {
        if (itmFrom === '') {
          window.location.href = ('/' + searchPageRedirectURL.pageRedirect + '&query=' + props.keyword)
        } else {
          window.location.href = ('/' + searchPageRedirectURL.pageRedirect + '&itm_source=search&itm_campaign=' + itmFrom + '&query=' + props.keyword)
        }
      } else pushRouterSearch(props.keyword)
    }
  }, [search])

  useEffect(() => {
    if (props.isClickedSearchBar) {
      dispatch(LogActions.popularSearchRequest('custom_popular_search'))
    }
  }, [])

  const pushRouterCategory = (suggest, urlKey, event, categoryName, isRuleBased = false, categoryId = '') => {
    event.preventDefault()
    const keyword = props.keyword.toLowerCase().trim() || ''
    const itm = '?itm_source=search&itm_campaign=suggestion&itm_term=' + categoryName
    const query = { category: urlKey, keyword, pageType: 'category', isRuleBased, categoryId }
    router.push(
      url.format({
        pathname: '/catalog',
        query: query
      }),
      '/' + urlKey + itm + '&keyword=' + encodeURIComponent(keyword) + '&query=' + encodeURIComponent(keyword)
    )
  }

  const pushRouterSearch = (keyword) => {
    const typo = ''
    const showAll = false
    const itmFrom = ''
    // this.handleCookies(keyword)
    // this.props.categoryDetailInit()
    if (keyword) {
      keyword = keyword.replace(/ +/g, ' ').toLowerCase().trim()
      let itm = '?itm_source=search&itm_campaign=suggestion&itm_term=' + keyword + '&'
      if (itmFrom !== '') {
        itm = '?itm_source=search&itm_campaign=' + itmFrom + '&itm_term=' + keyword + '&'
      } else if (showAll) {
        itm = '?itm_source=search&itm_campaign=showAll&itm_term=' + keyword + '&'
      }
      router.push(
        url.format({
          pathname: '/algoliaCatalog',
          query: { keyword, pageType: 'category', isRuleBased: 'false', categoryId: '', typo, itmFrom }
        }),
        config.entryPoint + '/jual/' + encodeURIComponent(keyword.replace(/[^0-9A-Za-z ]/gi, '').replace(/ /g, '-').replace(/-+/g, '-')) + itm + 'keyword=' + encodeURIComponent(keyword) + '&query=' + encodeURIComponent(keyword)
      )
    }
  }

  const renderPopulerSearch = () => {
    if (log?.popularSearch?.pop) {
      return log?.popularSearch?.pop?.map((item, index) => {
        return (
          <li key={index}>
            <a onClick={() => {
              searchPageRedirect(item); searchProductByKeywordAlgolia(encodeURIComponent(item))
            }}
            >
              {item}
            </a>
          </li>
        )
      }
      )
    } else return null
  }

  const renderAutoSuggestions = () => {
    if (searchData && searchData.sug) {
      return searchData.sug?.map((product, index) => {
        if (index < 6) return <li key={index}><a href='javascript:;' onClick={() => { searchPageRedirect(product.query); searchProductByKeywordAlgolia(encodeURIComponent(product.query)) }}><b>{product.query}</b></a></li>
        else return null
      }
      )
    } else return null
  }

  const renderCategoriesRelated = () => {
    if (searchData && searchData.facets) {
      return (
        <>
          {searchData.facets?.map((facet, index) => {
            if (facet !== 'Temporary') {
              return (
                <li key={index}>
                  <a
                    href={config.baseURL + searchData.url[index] + '?itm_source=search&itm_campaign=suggestion&itm_term=' + facet + '&keyword=' + encodeURIComponent(props.keyword) + '&query=' + encodeURIComponent(props.keyword)}
                    onClick={(e) => { pushRouterCategory(props.keyword, searchData.url[index], e, facet); this.hideSearchBar() }}
                  >
                    <span>{props.keyword} di kategori <b>{facet}</b></span>
                  </a>
                </li>
              )
            } else return null
          }
          )}
        </>
      )
    } else return null
  }

  const renderProductCard = () => {
    if (searchData && searchData.prd) {
      return (
        searchData.prd?.map((product, index) => {
            let label = ''
            let isInStock = ''
            let infoDeliv = ''
            if (config.companyCode === 'HCI') {
              isInStock = product.is_in_stock.HCI
              label = product.label.HCI
            } else if (config.companyCode === 'AHI') {
              isInStock = product.is_in_stock.HCI
              label = product.label.AHI
            } else {
              isInStock = product.is_in_stock.ODI
              label = product.label.ODI
            }
            if (product.can_gosend) {
              if (product.can_gosend.length === 2) infoDeliv = 'sameday & instant'
              else infoDeliv = product.can_gosend[0]
            }
            return (
              <li key={index}>
                <a href={`${config.baseURL}${product.url_key}`}>
                  <span>{startCase(toLower(product.name))}</span>
                </a>
              </li>
            )
          })
      )
    }
  }

  return (
    <div className='card-body search-card'>
      <div className='row'>
        {props.keyword !== ''
          ? (props.keyword?.length < 2)
            ? (
              <div className='col-xs-12 '>
                <ul>
                  <li>Pencarian minimal 2 huruf</li>
                </ul>
              </div>
            )
            : (
              <div className='col-xs-12 scrollable'>
                <ul>
                  {renderAutoSuggestions()}
                  {renderCategoriesRelated()}
                  {renderProductCard()}
                </ul>
              </div>
            )
          : (
            <div className='scrollable'>
              <div className='col-xs-12 padding-m'>
                <div className='padding-bottom-s heading-2'>Pencarian Populer</div>
                <div className='padding-bottom-m'>
                  <ul className='pills'>
                    {renderPopulerSearch()}
                  </ul>
                </div>
              </div>
              <div className='padding-bottom-s heading-2' />
            </div>
          )}
      </div>
    </div>
  )
}
